import React, { useState } from 'react';
import './App.css';
import { useMutation, useQuery } from '@apollo/client';
import FormInput from './components/FormInput/FormInput';
import { GET_LIST_MENU } from './graphql/queries';
import { ADD_BOOK_MUTATION } from './graphql/mutations';
import { Box, Button } from '@mui/material';

interface IBook {
  name: string,
  genre: string,
  author: string[],
}

interface IAuthor {
  id: number,
  name: string,
  age: number,
  books: string[],
}

const App = (): JSX.Element => {
  const [value, setValue] = useState({
    name: "",
    genre: "",
    authorId: ""
  });

  const { loading, error, data } = useQuery(GET_LIST_MENU);
  const [addBook] = useMutation(ADD_BOOK_MUTATION, {
    refetchQueries: [
      {query: GET_LIST_MENU},
      "books"
    ]
  });

  const handleAddBook = () => {
    addBook({
      variables: {
        name: value.name,
        genre: value.genre,
        authorId: value.authorId
      }
    });
  };

  const handleChange = (event: any) => {
    setValue({...value, [event.target.name]: event.target.value});
  };
  
  return (
    <div className="App">
      <Box
        component="form"
        sx={{
          mt: "10px"
        }}
      >
        <FormInput
          name="name"
          type="text"
          label="Name"
          value={value.name}
          onChange={handleChange}
        />
        <FormInput
          name="genre"
          type="text"
          label="Genre"
          value={value.genre}
          onChange={handleChange}
        />
        <FormInput
          name="authorId"
          type="text"
          label="Author ID"
          value={value.authorId}
          handleChange={handleChange}
        />
        <Button variant="contained" onClick={handleAddBook} >Submit</Button>
      </Box>
      <ul>
        {
          !loading ? (
            data.books.map((data: IBook) => {
              return <li>{data.name}</li>
            })
          ) : (
            <div>Loading...</div>
          )
        }
      </ul>
    </div>
  );
}

export default App;
