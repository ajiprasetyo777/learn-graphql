import { gql } from '@apollo/client';

export const GET_LIST_MENU = gql`
  query {
    listMenu {
      name
      price
      type
      rating{
        type
      }
    }
  }
`;
