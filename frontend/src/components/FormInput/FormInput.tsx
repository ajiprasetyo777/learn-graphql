import React from 'react'
import { TextField } from "@mui/material";

interface IFormInputProps {
  name: string
  type: string
  label: string
  value: string
  required?: boolean
  onChange?: (event: any) => void
  handleChange?: (event: any) => void
}

const FormInput = ({
  handleChange,
  label,
  ...otherProps
}: IFormInputProps): JSX.Element => {  
  return (
    <div className="group">
        <TextField
            id="form-input"
            onChange={handleChange}
            label={label}
            {...otherProps}
        />
    </div>
  )
}

export default FormInput;