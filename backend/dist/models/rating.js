"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ratingSchema = new mongoose_1.Schema({
    menuId: String,
    score: Number,
});
exports.default = (0, mongoose_1.model)("Rating", ratingSchema);
//# sourceMappingURL=rating.js.map