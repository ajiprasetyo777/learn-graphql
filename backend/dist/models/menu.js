"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const menuSchema = new mongoose_1.Schema({
    name: String,
    price: Number,
    type: String,
});
exports.default = (0, mongoose_1.model)("Menu", menuSchema);
//# sourceMappingURL=menu.js.map