"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const express_graphql_1 = require("express-graphql");
const app = (0, express_1.default)();
const port = 3000;
app.use("/graphql", (0, express_graphql_1.graphqlHTTP)({}));
app.get("/", (req, res) => {
    res.send("Hello World! Aji a");
});
app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
});
//# sourceMappingURL=app.js.map