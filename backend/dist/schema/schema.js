"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const menu_1 = __importDefault(require("../models/menu"));
const rating_1 = __importDefault(require("../models/rating"));
const MenuType = new graphql_1.GraphQLObjectType({
    name: "Menu",
    fields: () => ({
        id: { type: graphql_1.GraphQLID },
        name: { type: graphql_1.GraphQLString },
        price: { type: graphql_1.GraphQLString },
        type: { type: graphql_1.GraphQLString },
        rating: {
            type: RatingType,
            resolve(parent, args) {
                return rating_1.default.findById(parent.menuId);
            }
        }
    })
});
const RatingType = new graphql_1.GraphQLObjectType({
    name: "Rating",
    fields: () => ({
        id: { type: graphql_1.GraphQLID },
        menuId: { type: graphql_1.GraphQLID },
        score: { type: graphql_1.GraphQLInt },
    })
});
//initial root query
const RootQuery = new graphql_1.GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        menu: {
            type: MenuType,
            args: { id: { type: graphql_1.GraphQLID } },
            resolve(parent, args) {
                // return  _.find(books, { id: args.id });
                // code to get data from db / other source
                return menu_1.default.findById(args.id);
            }
        },
        rating: {
            type: RatingType,
            args: { id: { type: graphql_1.GraphQLID } },
            resolve(parent, args) {
                // return  _.find(authors, { id: args.id });
                // code to get data from db / other source
                return rating_1.default.findById(args.id);
            }
        },
        listMenu: {
            type: new graphql_1.GraphQLList(MenuType),
            resolve(parent, args) {
                return menu_1.default.find({});
            }
        },
        // authors: {
        //     type: new GraphQLList(AuthorType),
        //     resolve(parent, args) {
        //         return Author.find({});
        //     }
        // }
    }
});
const Mutation = new graphql_1.GraphQLObjectType({
    name: "Mutation",
    fields: {
        // addAuthor: {
        //     type: AuthorType,
        //     args: {
        //         name: { type: new GraphQLNonNull(GraphQLString) },
        //         age: { type: new GraphQLNonNull(GraphQLInt) }
        //     },
        //     resolve(parent, args) {
        //         let author = new Author({
        //             name: args.name,
        //             age: args.age
        //         });
        //         return author.save();
        //     }
        // },
        addMenu: {
            type: MenuType,
            args: {
                name: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString) },
                price: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLInt) },
                type: { type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString) }
            },
            resolve(parent, args) {
                let menu = new menu_1.default({
                    name: args.name,
                    price: args.price,
                    type: args.type
                });
                return menu.save();
            }
        }
    }
});
exports.default = new graphql_1.GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
});
//# sourceMappingURL=schema.js.map