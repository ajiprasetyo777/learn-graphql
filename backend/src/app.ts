import express from "express";
import { graphqlHTTP } from "express-graphql";
import schema from "../schema/schema";
import mongoose from "mongoose";
import * as dotenv from "dotenv";
import cors from "cors";



dotenv.config();

const app = express();
const port = 8080;
const mongoString = process.env.DATABASE_URL;

mongoose.set("strictQuery", false);

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on("error", (error) => {
    console.log(error)
});

database.once("connected", () => {
    console.log("Database Connected");
});

app.use(cors({
    origin: "*"
}));

app.get("/", (req, res) => {
    res.send("Hello World! Aji");
});

app.use("/graphql", graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
})