import { 
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull
} from "graphql";
import _ from "lodash";
import Menu from '../models/menu';
import Rating from "../models/rating";

const MenuType = new GraphQLObjectType({
    name: "Menu",
    fields: () => ({
        id: { type: GraphQLID},
        name: { type: GraphQLString},
        price: { type: GraphQLString},
        type: { type: GraphQLString},
        rating: {
            type: RatingType,
            resolve(parent, args) {
                return Rating.findById(parent.menuId);
            }
        }
    })
})

const RatingType = new GraphQLObjectType({
    name: "Rating",
    fields: () => ({
        id: { type: GraphQLID },
        menuId: { type: GraphQLID },
        score: { type: GraphQLInt },
    }),
})

//initial root query
const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        menu: {
            type: MenuType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                // return  _.find(books, { id: args.id });
                // code to get data from db / other source
                return Menu.findById(args.id);
            }
        },
        rating: {
            type: RatingType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                // return  _.find(authors, { id: args.id });
                // code to get data from db / other source
                return Rating.findById(args.id);
            }
        },
        listMenu: {
            type: new GraphQLList(MenuType),
            resolve(parent, args) {
                return Menu.find({});
            }
        },
        // authors: {
        //     type: new GraphQLList(AuthorType),
        //     resolve(parent, args) {
        //         return Author.find({});
        //     }
        // }
    }
})

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        // addAuthor: {
        //     type: AuthorType,
        //     args: {
        //         name: { type: new GraphQLNonNull(GraphQLString) },
        //         age: { type: new GraphQLNonNull(GraphQLInt) }
        //     },
        //     resolve(parent, args) {
        //         let author = new Author({
        //             name: args.name,
        //             age: args.age
        //         });

        //         return author.save();
        //     }
        // },
        addMenu: {
            type: MenuType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                price: { type: new GraphQLNonNull(GraphQLInt) },
                type: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let menu = new Menu({
                    name: args.name,
                    price: args.price,
                    type: args.type
                });

                return menu.save();
            }
        },
        addRating: {
            type: RatingType,
            args: {
                menuId: { type: new GraphQLNonNull(GraphQLID) },
                score: { type: new GraphQLNonNull(GraphQLInt)}
            },
            resolve(parent, args) {
                let rating = new Rating({
                    menuId: args.menuId,
                    score: args.score
                });

                return rating.save();
            }
        }
    }
})

export default new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
});