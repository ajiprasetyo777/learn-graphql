import { Schema, model } from 'mongoose';

const ratingSchema = new Schema({
    menuId: String,
    score: Number,
}); 

export default model("Rating", ratingSchema);