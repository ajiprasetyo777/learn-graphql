import { Schema, model } from 'mongoose';

const menuSchema = new Schema({
    name: String,
    price: Number,
    type: String,
});

export default model("Menu", menuSchema);